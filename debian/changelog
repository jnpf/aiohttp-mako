aiohttp-mako (0.4.0-3) UNRELEASED; urgency=medium

  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on python3-mako.
  * Bump debhelper from deprecated 9 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

  [ Joao Nobrega ]
  * Team upload.
  * Drop python3-nose dependency
  * Bump Standards Version to 4.7.0 (no changes needed)

 -- joao-pedro <joaopedrobsb3@gmail.com>  Mon, 29 Apr 2024 13:07:16 -0300

aiohttp-mako (0.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Fri, 17 Sep 2021 00:02:32 -0400

aiohttp-mako (0.4.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Piotr Ożarowski ]
  * New upstream release
  * Standards-version bumped to 4.5.0 (no other changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 16 Mar 2020 12:12:31 +0100

aiohttp-mako (0.0.1-1) unstable; urgency=low

  * Initial release (closes: 832978)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 30 Jul 2016 13:26:53 +0000
